# Team Members

* Seth Probert  
* Dylan Wickham  
* Rainey Anson  

# Idea 1
SELECT Statements  
Teach the user to effectively utilize SELECT statements to retrieve data.  
Access to data is critical for efficient function in a professional technical environment.  

# Idea 2
Relational Algebra  
Teach the user to calculate and write relational algebra expressions.  
Relation algebra is a conceptual cornerstone of relational database management systems.

# Idea 3
Functional Dependencies  
Teach the user what a functional dependency is, how to extract the dependencies and normal forms.
Efficient storage and querying of the databases made possible through effective design by utilizing functional dependencies.
